/** @module collections */

export { Queue } from "./src/collections/queue";
export { BinarySearchTree } from "./src/collections/binary-search-tree";
