import { assert } from "chai";
import { Action } from "../action";
import { ActionState } from "../enumerations/action-state";
import { IllegalStateChangeError } from "../errors/illegal-state-change-error";

/**
 * Gets an instance of an Action<number> in a given state
 */
async function getActionInState(state: ActionState): Promise<Action<number>>
{
	const action = new Action(async () => 1);

	switch (state)
	{
		case ActionState.Unmounted:
			break;
		case ActionState.Mounted:
			action.mount();
			break;
		case ActionState.Dropped:
			action.drop();
			break;
		case ActionState.Committed:
			action.mount();
			await action.commit();
			break;
		case ActionState.Resolved:
			action.mount();
			action.resolve(await action.commit());
			break;
		case ActionState.Rejected:
			action.mount();
			await action.commit();
			action.reject(new Error("Test Error"));
			break;
	}

	return action;
}

describe("Action", () => {
	describe("#mount()", () => {
		it("should successfully mount an unmounted action", async () => {
			const action = await getActionInState(ActionState.Unmounted);
			action.mount();
		});

		it("should fail to mount from any other state", async () => {
			const states = [
				ActionState.Dropped,
				ActionState.Mounted,
				ActionState.Committed,
				ActionState.Rejected,
				ActionState.Resolved,
			];
			for (const state of states) {
				const action = await getActionInState(state);
				assert.throws(action.mount.bind(action), IllegalStateChangeError);
			}
		});

		it("should call the mounted event listener", (done) => {
			getActionInState(ActionState.Unmounted).then((action) => {
				action.on(ActionState.Mounted, () => {
					done();
				});

				action.mount();
			});
		});
	});

	describe("#commit()", () => {
		it("should successfully commit on a mounted action", async () => {
			const action = await getActionInState(ActionState.Mounted);
			assert.equal(await action.commit(), 1);
		});

		it("should fail to mount from any other state", async () => {
			const states = [
				ActionState.Unmounted,
				ActionState.Dropped,
				ActionState.Committed,
				ActionState.Rejected,
				ActionState.Resolved,
			];
			for (const state of states) {
				const action = await getActionInState(state);
				assert.throws(action.commit.bind(action), IllegalStateChangeError);
			}
		});

		it("should call the committed event listener", (done) => {
			getActionInState(ActionState.Mounted).then((action) => {
				action.on(ActionState.Committed, () => {
					done();
				});

				// Fire and forget
				action.commit();
			});
		});
	});

	describe("#drop()", () => {
		it("should successfully drop an unmounted or mounted action", async () => {
			const states = [
				ActionState.Unmounted,
				ActionState.Mounted,
			];
			for (const state of states) {
				const action = await getActionInState(state);
				action.drop();
			}
		});

		it("should fail to drop from any other state", async () => {
			const states = [
				ActionState.Dropped,
				ActionState.Committed,
				ActionState.Rejected,
				ActionState.Resolved,
			];
			for (const state of states) {
				const action = await getActionInState(state);
				assert.throws(action.drop.bind(action), IllegalStateChangeError);
			}
		});

		it("should call the dropped event listener", (done) => {
			getActionInState(ActionState.Unmounted).then((action) => {
				action.on(ActionState.Dropped, () => {
					done();
				});

				action.drop();
			});
		});

		it("should call the dropped event listener", (done) => {
			getActionInState(ActionState.Mounted).then((action) => {
				action.on(ActionState.Dropped, () => {
					done();
				});

				action.drop();
			});
		});
	});

	describe("#resolve", () => {
		it("should successfully resolve a committed action", async () => {
			const action = await getActionInState(ActionState.Committed);
			action.resolve(1);
		});

		it("should fail to resolve from any other state", async () => {
			const states = [
				ActionState.Dropped,
				ActionState.Unmounted,
				ActionState.Mounted,
				ActionState.Resolved,
				ActionState.Rejected,
			];
			for (const state of states) {
				const action = await getActionInState(state);
				assert.throws(action.resolve.bind(action, 1), IllegalStateChangeError);
			}
		});

		it("should call the resolved event listener", (done) => {
			getActionInState(ActionState.Mounted).then((action) => {
				action.on(ActionState.Resolved, (value) => {
					assert.equal(value, 1);
					done();
				});

				action.commit().then((value) => {
					// Fire and forget
					action.resolve(value);
				});
			});
		});
	});
});
