/** @module action */

import { IAction } from "./action";

/**
 * Interface for Action Managers. Any data structure implemented as an action
 * manager must implement this interface to support the {@link Action.pushInto}
 * method.
 *
 * @typeParam T	Return type of actions being managed by this instance
 */
export interface IActionManager<T>
{
	/**
	 * Mount an Action into this manager with its default method of adding
	 * actions.
	 *
	 * @remarks
	 *
	 * While Action Managers will likely vary pretty wildly in function and
	 * implementation, they will all have some way to add actions since
	 * otherwise the entire usage of an Action would be redundant. An example of
	 * this method for something like a simple Queue would be pushing the Action
	 * onto the end while omething like a Max-Queue might push it into the
	 * middle depending on some ordering condition.
	 *
	 * @param item	The Action to add to this manager
	 */
	push(item: IAction<T>): Promise<void>;
}
