/** @module action */

/**
 * Represents an object that needs to run a subroutine when it is "dropped" in
 * a similar manner to the std::ops::Drop trait in Rust or Destructors in C++.
 *
 * @remarks
 *
 * Since JavaScript has no concept of Destructors and this interface is totally
 * non-standard; this is just used by the Action classes in this library to
 * indicate that a particular action has been dropped off of its manager and
 * will never commit to its promise. Any other usage has no guarantee of being
 * anything like what is described above.
 */
export interface IDrop
{
	/**
	 * Called when this object has been "forgotten" and will likely be garbage
	 * collected sometime after this method finishes.
	 *
	 * @remarks
	 *
	 * There is no standard for the usage of this method outside of this library
	 * so behavior may vary if this gets called by anything outside of the Void
	 * Action library.
	 */
	drop(): void;
}
