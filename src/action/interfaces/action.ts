/** @module action */

import { IDrop } from "./drop";

/**
 * Inteface for receiving lifecycle events of an Action.
 *
 * @remarks
 *
 * These should be called by the {@link IActionManager} but that behavior may
 * vary between implementations. See the documentation for the specific manager
 * implementation for its supported lifecycle events.
 *
 * ## See Also
 *
 * - {@link ActionState} for more information on the different states of
 * an Action's lifecycle.
 *
 * @typeParam T	Type of the value returned by promise should it be executed
 */
export interface IAction<T> extends IDrop
{
	/**
	 * Called when this Action has been pushed into an Action Manager
	 */
	mount(): void;

	/**
	 * Called when the Action Manager has committed to this Action and the
	 * stored promise should be executed.
	 *
	 * @returns The stored promise
	 */
	commit(): Promise<T>;

	/**
	 * Called when the stored promise returns successfully.
	 *
	 * @param response	The value returned by the promise
	 */
	resolve(response: T): void;

	/**
	 * Called when the stored promise throws an error.
	 *
	 * @param error	The thrown error
	 */
	reject(error: any): void;
}
