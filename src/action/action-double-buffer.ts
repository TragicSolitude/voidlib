/** @module action */

import { Nullable } from "../types";
import { IAction } from "./interfaces/action";
import { IActionManager } from "./interfaces/action-manager";

/**
 * An implementation of {@link IActionManager} for managing {@link Action} in a
 * double buffer. This will commit one action at a time and once the current
 * action resolves or rejects it will commit to only the last pushed action.
 *
 * @remarks
 *
 * This is great for UIs regarding debouncing inputs. If for example you have a
 * search field that calls an API on keydown you can put that API call into an
 * action and push that action into an instance of this class. Only one API call
 * will be waiting at any given point and only after that call finishes will
 * another be made.
 *
 * @typeParam T	Return type of managed actions
 */
export class ActionDoubleBuffer<T> implements IActionManager<T>
{
	/**
	 * The currently committed action (if any)
	 */
	private current: Nullable<IAction<T>>;

	/**
	 * The next action to commit to once the current one has completed. This
	 * action will be overridden if another is pushed before the current action
	 * has completed yet.
	 */
	private next: Nullable<IAction<T>>;

	/**
	 * Default constructor
	 */
	constructor()
	{
		this.current = null;
		this.next = null;
	}

	/**
	 * Pushes an item into either the {@link current} buffer or the {@link next}
	 * buffer. If the current buffer is occupied then the given action is put
	 * into the next buffer, displacing any current action (and subsequently
	 * triggering the {@link drop} method on that action. Otherwise the item is
	 * put into the current buffer and committed immediately.
	 *
	 * @param item	The action to buffer
	 */
	public push(item: IAction<T>): Promise<void>
	{
		item.mount();
		if (this.current === null)
		{
			this.current = item;
			return this.commitTo(item);
		}
		else
		{
			if (this.next !== null)
			{
				this.next.drop.call(this.next);
			}
			this.next = item;
		}

		return Promise.resolve();
	}

	/**
	 * Commit to the given action.
	 *
	 * @param item	The action to commit to
	 */
	private async commitTo(item: IAction<T>): Promise<void>
	{
		try
		{
			item.resolve(await item.commit());
		}
		catch (error)
		{
			item.reject(error);
		}

		this.current = this.next;
		this.next = null;

		if (this.current !== null)
		{
			return this.commitTo(this.current);
		}
	}
}
