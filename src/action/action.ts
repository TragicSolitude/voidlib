/** @module action */

import { ActionState } from "./enumerations/action-state";
import { IllegalStateChangeError } from "./errors/illegal-state-change-error";
import { IAction } from "./interfaces/action";
import { IActionManager } from "./interfaces/action-manager";

/**
 * Reference implementation of {@link IAction}. This implementation just
 * forwards lifecycle events to attached event listeners and is likely all that
 * will be needed and just provides a nice chainable function for signing up
 * event listeners to handle various lifecycle events.
 *
 * @remarks
 *
 * In this library and this class, the main concepts and terms used are:
 * - **Action**
 *
 *   A representation of a task that may or may not ever execute, while a
 *   Promise object is a function that executes now and promises to return
 *   later, an Action is a function that may execute later and return later
 *   than that or it may not execute at all.
 *
 * - **Lifecycle**
 *
 *   Here lifecycle refers to the lifecycle of an Action, or more specifically
 *   the various states and state transitions for an Action. For example a full
 *   lifecycle of some Action may be: `(start)-construct()--> [Unmounted]
 *   -mount()--> [Mounted] -drop()--> [Dropped]` where the final state is
 *   `[Dropped]`. In this example the Action was dropped before executing its
 *   Stored Promise.
 *
 * - **Stored Promise**
 *
 *   A Stored Promise is a fancy way of referring to a function that returns a
 *   promise that makes up the base model for Actions.
 *
 * - **Action Manager**
 *
 *   An Action Manager is the part of the Action library that actually *does*
 *   stuff. It is left intentionally vague so as to allow implementations to be
 *   flexible but ultimately an Action Manager is something that moves Actions
 *   through their lifecycles in some pattern.
 *
 * ## See Also
 * - {@link ActionState} for more details on the lifecycle events for an Action
 * - {@link IActionManager} for more details on Action Managers
 * - {@link ActionRingBuffer} for an example of an Action Manager
 *
 * @typeParam T	Type of value ultimately returned by stored promise
 */
export class Action<T> implements IAction<T>
{
	/**
	 * Current state of this Action in the Action lifecycle
	 */
	private state: ActionState;
	/**
	 * The promise to maybe be executed by this action. There is no guarantee
	 * that this promise will be executed.
	 */
	private payload: () => Promise<T>;
	/**
	 * Mapping of event listeners to their corresponding event types.
	 *
	 * @privateRemarks
	 *
	 * Strict typing for the callbacks stored here is handled by the
	 * {@link Action.on} method variations.
	 */
	private listeners: {[K in ActionState]?: (...args: any[]) => void};

	/**
	 * Default constructor; initializes a basic action using the given stored
	 * promise.
	 *
	 * @param payload	Stored promise to initialize this action with
	 */
	constructor(payload: () => Promise<T>)
	{
		this.state = ActionState.Unmounted;
		this.payload = payload;
		this.listeners = {};
	}

	/**
	 * Lifecycle event handler for the {@link ActionState.Mounted} state. This
	 * implementation just calls the associated state event listener if one is
	 * attached.
	 *
	 * @throws	IllegalStateChangeError If this action isn't Unmounted, e.g. it
	 * 			is already mounted.
	 */
	public mount(): void
	{
		if (this.state !== ActionState.Unmounted)
		{
			throw new IllegalStateChangeError();
		}

		this.state = ActionState.Mounted;
		if (ActionState.Mounted in this.listeners)
		{
			this.listeners[ActionState.Mounted]!.call(this);
			delete this.listeners[ActionState.Mounted];
		}
	}

	/**
	 * Lifecycle event handler for the {@link ActionState.Committed} state.
	 *
	 * @throws	IllegalStateChangeError If this action isn't yet mounted
	 * @returns	The promise extracted from the stored promise used to construct
	 * 			this object.
	 */
	public commit(): Promise<T>
	{
		if (this.state !== ActionState.Mounted)
		{
			throw new IllegalStateChangeError();
		}

		this.state = ActionState.Committed;
		if (ActionState.Committed in this.listeners)
		{
			this.listeners[ActionState.Committed]!.call(this);
			delete this.listeners[ActionState.Committed];
		}
		return this.payload();
	}

	/**
	 * Lifecycle event handler for the {@link ActionState.Resolved} state.
	 *
	 * @throws	IllegalStateChangeError If this action isn't committed
	 */
	public resolve(response: T): void
	{
		if (this.state !== ActionState.Committed)
		{
			throw new IllegalStateChangeError();
		}

		this.state = ActionState.Resolved;
		if (ActionState.Resolved in this.listeners)
		{
			this.listeners[ActionState.Resolved]!.call(this, response);
			delete this.listeners[ActionState.Resolved];
		}
	}

	/**
	 * Lifecycle event handler for the {@link ActionState.Rejected} state.
	 *
	 * @throws IllegalStateChangeError If this action isn't committed
	 */
	public reject(error: any): void
	{
		if (this.state !== ActionState.Committed)
		{
			throw new IllegalStateChangeError();
		}

		this.state = ActionState.Rejected;
		if (ActionState.Rejected in this.listeners)
		{
			this.listeners[ActionState.Rejected]!.call(this, error);
			delete this.listeners[ActionState.Rejected];
		}
	}

	/**
	 * Lifecycle event handler for the {@link ActionState.Dropped} state.
	 *
	 * @throws	IllegalStateChangeError If this action is not in a point where
	 * 			it can be dropped (e.g. it has been committed or resolved
	 * 			already).
	 */
	public drop(): void
	{
		const invalidState = true
			&& this.state !== ActionState.Unmounted
			&& this.state !== ActionState.Mounted;
		if (invalidState)
		{
			throw new IllegalStateChangeError();
		}

		this.state = ActionState.Dropped;
		if (ActionState.Dropped in this.listeners)
		{
			this.listeners[ActionState.Dropped]!.call(this);
			delete this.listeners[ActionState.Dropped];
		}
	}

	/**
	 * Convenience method for pushing this Action into a manager; good for
	 * ending {@link Action.on} chains.
	 *
	 * ## Example
	 *
	 * ```ts
	 * const manager = new ActionDoubleBuffer<number>();
	 *
	 * // Without .pushInto()
	 * const action = new Action<number>(async () => 1337)
	 *     .on(ActionState.Resolved, value => {
	 *         console.log(value);
	 *     });
	 * manager.push(action);
	 *
	 * // With .pushInto()
	 * new Action<number>(async () => 1337)
	 *     .on(ActionState.Resolved, value => {
	 *         console.log(value);
	 *     })
	 *     .pushInto(manager);
	 * ```
	 */
	public pushInto(manager: IActionManager<T>): void
	{
		manager.push(this);
	}

	/**
	 * A chainable method for attaching an event listener to a particular state.
	 * The arguments passed to the event listener varies between states. Only
	 * one event listener can be assigned to each state and is called when the
	 * Action has entered that state.
	 *
	 * @remarks
	 *
	 * The callbacks always return null. Furthermore, most of the events are
	 * going to have no useful information associated with them so the callback
	 * for a given state is most likely going to take no arguments. The notable
	 * states that do have arguments are the {@link ActionState.Resolved} and
	 * {@link ActionState.Rejected} states as those coincide with the standard
	 * callbacks for the completion of a Promise.
	 *
	 * @param e			The state entrance to listen for
	 * @param callback	Callback for given state
	 * @returns Current object for chaining
	 */
	public on(e: ActionState.Mounted, callback: () => void): this;
	public on(e: ActionState.Committed, callback: () => void): this;
	public on(e: ActionState.Resolved, callback: (response: T) => void): this;
	public on(e: ActionState.Rejected, callback: (error: any) => void): this;
	public on(e: ActionState.Dropped, callback: () => void): this;
	public on(e: ActionState, callback: (...args: any[]) => void): this
	{
		this.listeners[e] = callback;
		return this;
	}
}
