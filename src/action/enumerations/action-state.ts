/** @module action */

/**
 * The states of an Action's lifecycle
 */
export const enum ActionState
{
	/**
	 * Action has been created but has not been mounted into any Action
	 * Managers. The action will never commit to its promise.
	 */
	Unmounted,
	/**
	 * Action has been mounted in an Action Manager and *may* commit to its
	 * promise in the future.
	 */
	Mounted,
	/**
	 * Action is committed to its promise and *will* execute it in the future or
	 * is already executing it.
	 */
	Committed,
	/**
	 * Promise returned successfully
	 */
	Resolved,
	/**
	 * Promise threw an error
	 */
	Rejected,
	/**
	 * Action has been dropped off of the Action Manager and will never
	 * commit to its promise.
	 */
	Dropped,
}
