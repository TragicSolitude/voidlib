/** @module action */

/**
 * Error thrown when attempting to make an illegal state change in a state
 * machine.
 */
export class IllegalStateChangeError extends Error
{
	/**
	 * Default constructor
	 */
	constructor()
	{
		super("Attempted to make an illegal state change in a state machine");

		this.name = "IllegalStateChangeError";
	}
}
