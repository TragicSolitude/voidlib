/** @module action */

import { IAction } from "./interfaces/action";
import { IActionManager } from "./interfaces/action-manager";

/**
 * An implementation of {@link IActionManager} for managing {@link Action} in a
 * queue. This will execute one action at a time in order of first-in-first-out.
 *
 * @remarks
 *
 * This class will always commit to an action.
 *
 * @typeParam T Return type of managed actions
 */
export class ActionQueue<T> implements IActionManager<T>
{
	/**
	 * Backing Queue structure
	 *
	 * @todo Implement delayed shift queue.
	 */
	private queue: Array<IAction<T>>;

	/**
	 * Default constructor
	 */
	constructor()
	{
		this.queue = [];
	}

	/**
	 * Pushes an item onto the queue. It will be committed when any existing
	 * actions that were pushed to the queue previously have completed.
	 *
	 * @param item	The action to queue
	 */
	public push(item: IAction<T>): Promise<void>
	{
		item.mount();
		this.queue.push(item);
		if (this.queue.length === 1)
		{
			// Push the item to the queue even if its empty so that the
			// structure can track its existence without an extra field
			return this.pop();
		}

		return Promise.resolve();
	}

	/**
	 * "Pops" the next action off of the queue and starts running through the
	 * queue until it is empty
	 */
	private async pop(): Promise<void>
	{
		const item = this.queue[0];

		try
		{
			item.resolve(await item.commit());
		}
		catch (error)
		{
			item.reject(error);
		}

		this.queue.shift();

		if (this.queue.length > 0)
		{
			return this.pop();
		}
	}
}
