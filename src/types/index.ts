/** @module types */

/**
 * Type for values that may be null but never undefined.
 *
 * @remarks
 *
 * While 'Nullable<T>' is longer than 'T | null' it more clearly indicates what
 * is going on.
 *
 * This is best used with the "strictNullChecks" compiler flag enabled.
 */
export type Nullable<T> = T | null;

/**
 * Type for values that may not be defined at all. Make sure to check
 * for it's existence before accessing.
 *
 * @remarks
 *
 * In this library undefined and null are considered distinct things with
 * distinct meanings and semantics. Null indicates a *reference to* nothing;
 * undefined is an "instance" of nothing.
 *
 * Null will be returned when an object is blank/empty
 *
 * Undefined will be returned when nothing exists at all (e.g. trying to access
 * an unset array index, there is nothing there).
 */
export type Maybe<T> = T | undefined;
