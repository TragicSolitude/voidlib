import { assert } from "chai";
import { Queue } from "../queue";

describe("Queue", () => {
	describe("#new()", () => {
		it("should have a default constructor", () => {
			assert.instanceOf(new Queue(), Queue);
		});
	});

	describe("#enqueue()", () => {
		it("should enqueue and dequeue", () => {
			const a = new Queue();
			a.enqueue(1);
			a.enqueue(2);
			a.enqueue(3);

			assert.equal(a.dequeue(), 1);
			assert.equal(a.dequeue(), 2);
			assert.equal(a.dequeue(), 3);
		});
	});

	describe("#dequeue()", () => {
		it("should dequeue undefined when empty", () => {
			const a = new Queue();
			assert.equal(a.length, 0);
			assert.isUndefined(a.dequeue());
		});
	});

	describe("implements ICollection", () => {
		it("should have a length field", () => {
			const a = new Queue();
			a.enqueue(1);
			a.enqueue(2);
			a.enqueue(3);

			assert.equal(a.length, 3);
		});

		it("should have an iterator", () => {
			const a = new Queue();

			assert.exists(a[Symbol.iterator]);
		});

		it("should iterate in the correct order", () => {
			const a = new Queue();
			a.enqueue(1);
			a.enqueue(2);
			a.enqueue(3);

			const iterator = a[Symbol.iterator]();
			assert.equal(iterator.next().value, 1);
			assert.equal(iterator.next().value, 2);
			assert.equal(iterator.next().value, 3);
			assert.isTrue(iterator.next().done);
		});
	});
});
