import { assert } from "chai";
import { BinarySearchTree } from "../binary-search-tree";

describe("BinarySearchTree", () => {
	describe("#new()", () => {
		it("should have a default constructor", () => {
			assert.instanceOf(new BinarySearchTree(), BinarySearchTree);
		});
	});

	describe("implements IIndexable", () => {
		it("should set by keys", () => {
			// Numbers
			const tree = new BinarySearchTree();
			tree.set(1, "one");
			tree.set(2, "two");

			// Strings
			const tree2 = new BinarySearchTree();
			tree2.set("a", "one");
			tree2.set("b", "two");
		});

		it("should overwrite values with matching keys", () => {
			const tree = new BinarySearchTree();
			tree.set(1, "one");
			tree.set(2, "two");
			tree.set(2, "three");
			assert.equal(tree.length, 2);

			const tree2 = new BinarySearchTree();
			tree2.set("a", "one");
			tree2.set("b", "two");
			tree2.set("b", "three");
			assert.equal(tree2.length, 2);
		});

		it("should allow retrieval by key", () => {
			const tree = new BinarySearchTree();
			tree.set(1, "one");
			tree.set(5, "two");
			tree.set(3, "three");

			assert.equal(tree.get(5), "two");
			assert.isUndefined(tree.get(10));
		});

		it("should allow removal by key", () => {
			const tree = new BinarySearchTree();
			tree.set(1, "one");
			tree.set(5, "two");
			tree.set(3, "three");

			tree.del(5);
			assert.equal(tree.length, 2);
			assert.isUndefined(tree.get(5));
			assert.throws(tree.del.bind(tree), ReferenceError);
		});

		it("should allow finding the key of an item", () => {
			const tree = new BinarySearchTree();
			tree.set(9, "one");
			tree.set(4, "two");
			tree.set(8, "three");

			assert.equal(tree.key_of("two"), 4);
			assert.equal(tree.key_of("asdf"), undefined);
		});
	});

	describe("implements ICollection", () => {
		it("should have a length field", () => {
			const tree = new BinarySearchTree();
			tree.set(1, 1);
			tree.set(2, 2);
			tree.set(3, 3);
			assert.equal(tree.length, 3);
		});

		it("should have an iterator", () => {
			const tree = new BinarySearchTree();

			assert.exists(tree[Symbol.iterator]);
		});

		it("should iterate in the correct order", () => {
			// Hardcoded random list to keep test deterministic
			const keys = [0, 6, 1, 2, 9, 4, 1, 6, 8, 7, 6, 2, 1, 4, 8, 5, 6, 2];
			const tree = new BinarySearchTree<number, number>();
			for (const key of keys)
			{
				tree.set(key, key);
			}

			let last = -1;
			for (const value of tree)
			{
				assert.isAbove(value, last);
				last = value;
			}
		});

		it("should have a has method", () => {
			const tree = new BinarySearchTree();
			tree.set(4, "one");
			tree.set(2, "two");
			tree.set(100, "five");

			assert.exists(tree.has);
			assert.isTrue(tree.has("two"));
			assert.isFalse(tree.has("asdf"));
		});
	});

	describe("is a proper AVL tree", () => {
		it("should keep the backing array dense", () => {
			const tree = new BinarySearchTree();
			tree.set(1, "one");
			tree.set(2, "two");
			tree.set(3, "three");
			tree.set(5, "four");
			tree.set(8, "five");

			// A balanced tree should have worst case O(n + 1) space
			// tslint:disable-next-line:no-string-literal
			assert.isBelow(tree["list"].length, 7);
		});
	});
});
