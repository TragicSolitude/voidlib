/** @module collections */

import { ICollection } from "../interfaces/collection";
import { Maybe } from "../types/index";

/**
 * Queue
 */
export class Queue<T> implements ICollection<T>
{
	/**
	 * Internal backing data structure
	 */
	private list: T[];

	/**
	 * @inheritdoc
	 */
	public get length(): number {
		return this.list.length;
	}

	/**
	 * Default constructor
	 */
	constructor()
	{
		this.list = [];
	}

	/**
	 * Iterator
	 *
	 * @returns	An iterator over the backing array of this queue
	 */
	public [Symbol.iterator](): Iterator<T>
	{
		return this.list[Symbol.iterator]();
	}

	/**
	 * @inheritdoc
	 */
	public has(item: T): boolean
	{
		for (const thing of this.list)
		{
			if (thing === item)
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Enqueue
	 */
	public enqueue(item: T): void
	{
		this.list.push(item);
	}

	/**
	 * Dequeue
	 */
	public dequeue(): Maybe<T>
	{
		return this.list.shift();
	}
}
