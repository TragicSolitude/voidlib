import "../interfaces/indexable.se";

import { ICollection } from "../interfaces/collection";
import { IIndexable } from "../interfaces/indexable.se";
import { Maybe } from "../types/index";

/**
 * Transforms an index of a leaf to the index of its left child
 *
 * @param	index Current leaf
 * @returns	The index of the left child
 */
function left(index: number): number
{
	return 2 * index + 1;
}

/**
 * Transforms an index of a leaf to the index of its right child
 *
 * @param	index Current leaf
 * @returns	The index of the right child
 */
function right(index: number): number
{
	return 2 * index + 2;
}

/**
 * Transforms an index of a leaf to the index of its parent
 *
 * @param	index Current leaf
 * @returns	The index of the parent
 */
function parent(index: number): number
{
	return Math.floor((index - 1) / 2);
}

/**
 * Binary Tree
 */
export class BinarySearchTree<T, K> implements ICollection<K>, IIndexable<T, K>
{
	/**
	 * @inheritdoc
	 */
	public length: number;

	/**
	 * Internal backing array
	 *
	 * @remarks
	 *
	 * This class uses an array for the backing storage for better locality
	 * which often improves performance by reducing cache misses.
	 */
	private list: Array<[T, K]>;

	constructor()
	{
		this.list = [];
		this.length = 0;
	}

	/**
	 * Iterator
	 */
	public *[Symbol.iterator](): Iterator<K>
	{
		let i = 0;
		const list = this.list;

		// Seek to leftmost item in tree
		while (list[i] !== undefined)
		{
			i = left(i);
		}

		i = parent(i);

		while (list[i] !== undefined)
		{
			yield list[i][1];

			// If right child exists
			if (list[right(i)] !== undefined)
			{
				// Goto right child
				i = right(i);
				// Seek down to leftmost child
				while (list[left(i)] !== undefined)
				{
					i = left(i);
				}
			}
			else
			{
				// Seek up right side of subtree to first left child or root
				let prev_index;

				do
				{
					prev_index = i;
					i = parent(i);
				}
				while (i > -1 && prev_index % 2 === 0);
			}
		}
	}

	/**
	 * Checks if the given item exists in this tree
	 *
	 * @param	item The item to search for
	 * @returns True if this tree contains the given item
	 */
	public has(item: K): boolean
	{
		return this.key_of(item) !== undefined;
	}

	/**
	 * Gets an item at the given key
	 *
	 * @param	key The key to lookup
	 * @returns	The item at the given key or null if it doesn't exist
	 */
	public get(key: T): Maybe<K>
	{
		const index = this.seek(key);
		return this.list[index] && this.list[index][1];
	}

	/**
	 * Inserts an item with a given key
	 *
	 * @param	key - Key for item
	 * @param	item - Item to insert
	 */
	public set(key: T, item: K): void
	{
		const index = this.seek(key);
		const leaf = this.list[index];

		// TOOD Auto balance

		this.list[index] = [key, item];
		if (leaf === undefined)
		{
			this.length++;
		}
	}

	/**
	 * Removes the item at the given key from the tree
	 *
	 * @param	key The key to remove
	 * @throws	ReferenceError If the key does not exist
	 */
	public del(key: T): void
	{
		const index = this.seek(key);

		if (this.list[index] === undefined)
		{
			throw new ReferenceError("\"" + key + "\" is not set");
		}

		// TODO Delete
	}

	/**
	 * Get the index of the *first* instance of the given item is. Item
	 * equality is checked strictly (`===`).
	 *
	 * @param	item The item to search for
	 * @returns	The key of the given item or null if not found
	 */
	public key_of(item: K): Maybe<T>
	{
		return undefined;
	}

	/**
	 * Lookup the internal location of an item by its key
	 *
	 * @param	key Key to lookup
	 * @returns The internal index
	 */
	private seek(key: T): number
	{
		let index = 0;
		let leaf = this.list[index];

		while (leaf !== undefined)
		{
			if (key < leaf[0])
			{
				index = left(index);
			}
			else if (key > leaf[0])
			{
				index = right(index);
			}
			else
			{
				return index;
			}

			leaf = this.list[index];
		}

		return index;
	}
}
