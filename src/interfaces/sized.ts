/** @module interfaces */

/**
 * Something with a countable size.
 *
 * @remarks
 *
 * Based on Python's "Sized" ABC.
 */
export interface ISized
{
	/**
	 * The length of this instance
	 */
	length: number;
}
