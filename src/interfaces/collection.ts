/** @module interfaces */

import { IContainer } from "./container.se";
import { ISized } from "./sized";

/**
 * Combination interface for a collection of items.
 *
 * @remarks
 *
 * Based on Python's "Collection" ABC
 */
export interface ICollection<T> extends ISized, Iterable<T>, IContainer<T>
{}
