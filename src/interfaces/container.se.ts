/** @module interfaces */

/**
 * Something that contains something else
 *
 * @remarks
 *
 * Based on Python's "Container" ABC
 */
export interface IContainer<T>
{
	/**
	 * Contains the given instance of T
	 */
	has(item: T): boolean;
}

declare global
{
	// tslint:disable
	interface Array<T>
	{
		has(item: T): boolean;
	}
	// tslint:enable
}

/**
 * Implementation of IContainer for Arrays
 *
 * @param item	The item to search for
 * @returns true if `item` exists in this array
 */
function array_has<T>(this: T[], item: T): boolean
{
	return this.indexOf(item) > -1;
}

Array.prototype.has = array_has;
