/** @module interfaces */

import { Maybe } from "../types/index";

/**
 * Interface for a thing that is indexable
 */
export interface IIndexable<T, K>
{
	/**
	 * Gets an item at "index" T. In the case of non-array structures (HashMaps
	 * and the like), this index may be a string or something not congruent
	 * with an offset from the start of an array.
	 *
	 * @param	index The index to retrieve a value from
	 * @returns	The value located at the given index or null if it does not
	 * 			exist
	 */
	get(index: T): Maybe<K>;

	/**
	 * Sets an value at the given index. It is up to the implementor to decide
	 * what indexes are invalid and how to handle them.
	 *
	 * @param	index The "position" of an item
	 * @param	value The new value for the item at the given index
	 */
	set(index: T, value: K): void;

	/**
	 * Removes a value at the given index. It is up to the implementor to
	 * decide what indexes are invalid.
	 *
	 * @param	index The index of the item to remove
	 * @throws	ReferenceError If the item doesn't exist
	 */
	del(index: T): void;

	/**
	 * Get the index where the *first* instance of item is located.
	 *
	 * @param	item The item to look for
	 * @returns	The index of the given item or null if not found
	 */
	key_of(item: K): Maybe<T>;
}

declare global
{
	// tslint:disable
	interface Array<T>
	{
		get(index: number): Maybe<number>;
		set(index: number, value: T): void;
		del(index: number): void;
		key_of(item: T): Maybe<number>;
	}
}

/**
 * Implementation of {@link IIndexable.get} for Array bulitin.
 *
 * @param	index The index of an item
 * @returns An instance of T from the given index or undefined if it doesn't
 * 			exist.
 */
function array_get<T>(this: T[], index: number): Maybe<T>
{
	return this[index];
}

/**
 * Implementation of {@link IIndexable.set} for Array builtin.
 *
 * @param	index The index of the item to set
 * @param	value The new value for the given index
 */
function array_set<T>(this: T[], index: number, value: T): void
{
	this[index] = value;
}

/**
 * Implementation of {@link IIndexable.del} for Array builtin. While this does
 * remove the item from the array including its reference (allowing it to be
 * garbage collected if needed), the array indexes do not shift to reflect and
 * it's place is filled in with undefined.
 *
 * Example
 * ```
 * const a = [1, 2, 3];
 * delete a[1];
 *
 * // a is now [1, undefined, 3];
 * // a.length === 2
 * ```
 *
 * @param	index The index to remove from the array
 */
function array_del<T>(this: T[], index: number): void
{
	delete this[index];
}

/**
 * Implementation of {@link IIndexable.index} for Array builtin.
 *
 * @remarks
 *
 * It might be strange not to use `indexOf` in the interface so that Array can
 * automatically implement it. This was a concious descision to keep the
 * ability for custom implementations of the interface to allow for keys other
 * than numbers (strings for example). `Array.indexOf` returns -1 when an item
 * does not exist. However this interface requires that it return null because
 * an implementation may use an object that overrides `Object.valueOf` as its
 * key type.
 *
 * @param	item The item to look for
 * @returns	The index of the given item in the list or null if not found
 */
function array_key_of<T>(this: T[], item: T): Maybe<number>
{
	const index = this.indexOf(item);
	if (index === -1)
	{
		return undefined;
	}

	return index;
}

Array.prototype.get = array_get;
Array.prototype.set = array_set;
Array.prototype.del = array_del;
Array.prototype.key_of = array_key_of;
