# Voidlib

*The standard library typescript deserves, and the one it needs right now*

## Requirements
- Typescript 3.5
- JS Target ES2015 or newer

## Installation
**NPM installation only works with `--moduleResolution classic` and relative
paths or a bundler.**

Typescript will not emit .js files for .ts files that are within `node_modules/`
when using `--moduleResolution node`. Only option 1 can be used in this case.

1. Git Submodule:

	git submodule add https://gitlab.com/TragicSolitude/voidlib.git

2. NPM:

	npm install voidlib

## Usage
Everything the library exports is barrelled in a few top level Typescript
files. Importing is easy because this is nothing but a bunch of plain old
Typescript files. Make sure this repo is checked out somewhere accessible by
tsc and import it like you own it. The main problem arises from module
resolution and compilation.

If using `tsc`, `--moduleResolution classic`, and npm installation:

	`import { BinarySearchTree } from "/node_modules/voidlib/collections"`

If using `tsc`, `--moduleResolution classic`, and Git Submodule:

	`import { BinarySearchTree } from "/voidlib/collections"`

If using a bundler and `--moduleResolution node`:

	`import { BinarySearchTree } from "voidlib/collections"`
