/** @module interfaces */

import "./src/interfaces/container.se";
import "./src/interfaces/indexable.se";

export { IContainer } from "./src/interfaces/container.se";
export { ICollection } from "./src/interfaces/collection";
export { IIndexable } from "./src/interfaces/indexable.se";
export { ISized } from "./src/interfaces/sized";
